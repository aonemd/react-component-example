import React from "react";
import ReactDOM from "react-dom";

import Welcome from './components/Welcome';
import EarningsTable from './components/EarningsTable';

ReactDOM.render(<EarningsTable />, document.getElementById("root"));
